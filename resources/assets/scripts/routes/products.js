import pagination from '../modules/pagination';

export default {
  init() {
    // JavaScript to be fired on the product page
    pagination.startPagination();
  },
};

// AOS
import AOS from 'aos';

//tiny-slider
import {tns} from 'tiny-slider/src/tiny-slider.module';

// Back to top
import '../modules/back-to-top';
import menu from '../modules/menu';

import 'lity/dist/lity';

import lightbox from 'lightbox2';

export default {
  init() {
    // JavaScript to be fired on all pages
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
    menu.startMenu();
    AOS.init();

    if (jQuery('.c-hero__sliders').length > 0) {
      tns({
        container: '.c-hero__sliders',
        nav: true,
        controls: false,
        autoplay: true,
        autoplayButton: false,
        autoplayButtonOutput: false,
        autoplayTimeout: 9000,
        loop: true,
        smartSpeed: 6000,
        speed: 2000,
        responsiveClass: false,
        edgePadding: 0,
        autoWidth: false,
        items: 1,
        center: false,
      });
    }

    if (jQuery('.c-interiores__sliders').length > 0) {
      tns({
        container: '.c-interiores__sliders',
        items: 1,
        autoplay: true,
        autoplayButton: false,
        autoplayButtonOutput: false,
        controls: true,
        loop: true,
        nav: false,
        controlsText: ['<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 60 60" fill="none" class="img-svg replaced-svg"><rect width="50" height="50" fill="white"></rect><path fill-rule="evenodd" clip-rule="evenodd" d="M33.0776 22.3079L34.5527 23.783L28.3359 30.0005L34.5527 36.2174L33.0776 37.6925L25.3857 30.0005L33.0776 22.3079Z" fill="black"></path></svg>', '<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 60 60" fill="none" class="img-svg replaced-svg"><rect width="50" height="50" fill="white"></rect><path fill-rule="evenodd" clip-rule="evenodd" d="M26.8599 22.3079L25.3848 23.783L31.6016 30.0005L25.3848 36.2174L26.8599 37.6925L34.5518 30.0005L26.8599 22.3079Z" fill="black"></path></svg>'],
      });
    }

    lightbox.option({
      'showImageNumberLabel': false,
      'alwaysShowNavOnTouchDevices': false,
    });

  },
};
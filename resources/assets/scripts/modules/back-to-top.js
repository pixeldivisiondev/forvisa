let backToTopEl = jQuery('.js-back-to-top');

if (backToTopEl.length) {
  const scrollTrigger = backToTopEl.data('offset') ? backToTopEl.data('offset') : 100;
  const scrollDuration = backToTopEl.data('duration') ? backToTopEl.data('duration') : 700;

  let backToTop = function () {
    var scrollTop = jQuery(window).scrollTop();
    if (scrollTop > scrollTrigger) {
      backToTopEl.addClass('show');
    } else {
      backToTopEl.removeClass('show');
    }
  };

  backToTop();

  jQuery(window).on('scroll', function () {
    backToTop();
  });

  backToTopEl.on('click', function (e) {
    e.preventDefault();
    jQuery('html,body').animate({
      scrollTop: 0,
    }, scrollDuration);
  });
}
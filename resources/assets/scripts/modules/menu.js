/* eslint-disable */
function startMenu() {
    //var lastScroll = 0;
    var delta = 700;

    jQuery(window).bind('scroll mousewheel', function (event) {
        var scroll = jQuery(window).scrollTop();

        if (scroll > delta) {
            if (event.originalEvent.wheelDelta >= 0) {
                jQuery('.c-header--fixed').addClass('open');
            }
            else {
                jQuery('.c-header--fixed').removeClass('open');
            }
        } else {
            jQuery('.c-header--fixed').removeClass('open');
        }
    });

    var hamburguer = jQuery('.js-hamburger');
    var menu = jQuery('.js-menu');
    var header_logo = jQuery('.c-header__logo');

    hamburguer.on('click', function (e) {
        e.preventDefault();

        if (hamburguer.hasClass('open')) {
            hamburguer.removeClass('open');
            menu.removeClass('show');
            header_logo.removeClass('menu-open');
        } else {
            hamburguer.addClass('open');
            menu.addClass('show');
            header_logo.addClass('menu-open');
        }
    });
}

module.exports = {
    startMenu: startMenu,
};

jQuery(document).ready(function ($) {

    var menu_children_link = jQuery('.menu-item-has-children > a');
    menu_children_link.append(' <i class="u-icon-chevron-down"></i>');

    var menu_children = jQuery('.menu-item-has-children');
    var submenu = jQuery('.sub-menu');

    if (jQuery(window).width() > 1025) {
      menu_children.hover(function () {
        jQuery(this).find('.sub-menu').toggleClass("open-submenu-desktop");
      });
    }

});


jQuery(document).ready(function () {

  if (jQuery(window).width() < 1025) {

    jQuery('#menu-nav .sub-menu').hide(); //Hide children by default

    jQuery('#menu-nav li.menu-item-has-children > a').click(function () {
      event.preventDefault();
      jQuery(this).siblings('.sub-menu').slideToggle('slow');
    });
  }

});
/*jQuery(document).ready(function () {
  jQuery('.ancla-menu a').click(function(e){
    //e.preventDefault();
    var strAncla=$(this).attr('href'); //id del ancla
    jQuery('body,html').stop(true,true).animate({
      scrollTop: jQuery(strAncla).offset().top + (-55)
    },1000);

  });
});*/

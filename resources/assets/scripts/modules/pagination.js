function startPagination() {

  $(document).ready(function () {
    setTimeout(function () {
      var actualBottom = (jQuery('.c-productos__container').offset().top + jQuery('.c-productos__container').outerHeight(true)) - 700;
      var delta = jQuery('.c-pagination').outerHeight() - 250;
      var scrollinit = jQuery(window).scrollTop();
      if (scrollinit > delta) {
        jQuery('.c-pagination').addClass('c-pagination--fixed');
      }
      jQuery(window).bind('scroll mousewheel', function () {
        var scroll = jQuery(window).scrollTop();

        if (scroll > actualBottom) {
          jQuery('.c-pagination').removeClass('c-pagination--fixed');
        }

        if ((scroll > delta) && (scroll < actualBottom)) {
          jQuery('.c-pagination').addClass('c-pagination--fixed');
        } else {
          jQuery('.c-pagination').removeClass('c-pagination--fixed');
        }
      });
    }, 100);
  });
}

module.exports = {
  startPagination: startPagination,
};

function startMenu() {
    //var lastScroll = 0;
    var delta = 700;

    jQuery(window).bind('scroll mousewheel', function (event) {
      var scroll = jQuery(window).scrollTop();

      if (scroll > delta) {
        if (event.originalEvent.wheelDelta >= 0) {
          jQuery('.c-header--fixed').addClass('open');
        }
        else {
          jQuery('.c-header--fixed').removeClass('open');
        }
      } else {
        jQuery('.c-header--fixed').removeClass('open');
      }
    });

    var hamburguer = jQuery('.js-hamburger');
    var menu =  jQuery('.js-menu');
    var header_logo =  jQuery('.c-header__logo');

    hamburguer.on('click', function (e) {
      e.preventDefault();

      if (hamburguer.hasClass('open')) {
        hamburguer.removeClass('open');
        menu.removeClass('show');
        header_logo.removeClass('menu-open');
      } else {
        hamburguer.addClass('open');
        menu.addClass('show');
        header_logo.addClass('menu-open');
      }
    });
  }

  module.exports = {
    startMenu: startMenu,
  };

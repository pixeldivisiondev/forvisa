{{--
  Template Name: Texto Template
--}}

@extends('layouts.app')

@section('content')

 @include('partials.text.hero')
 @include('partials.text.content')
  
@endsection

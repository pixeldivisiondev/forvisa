@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.categoria.content-single-categoria')
  @endwhile
@endsection

{{-- HERO --}}
@include('partials.principales.hero')
{{-- breadcrumbs --}}
<div class="c-interiores__breadcrumbs">
    @include('partials.breadcrumbs.content')
</div>
{{-- bloque-a --}}
@include('partials.interiores.bloques.bloque-a')

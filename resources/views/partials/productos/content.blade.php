<section class="o-section">
    <div class="c-productos">
      @include('partials.pagination')
        <div class="c-productos__container u-wrapper">
            @foreach ($productos as $producto)
                @php
                    $imagen_producto = 'images/productos/producto/'. $producto->ID . '.jpg';
                    $imagen_tabla = 'images/productos/tabla/'. $producto->ID . '.jpg';
                    $titulo_personalizado = get_field('titulo_producto', $producto->ID);
                @endphp

                <div class="c-productos__bloque">
                <div class="c-productos__titulo">
                  @if($titulo_personalizado != '')
                    {{ $titulo_personalizado }}
                  @else
                    {{ $producto->post_title }}
                  @endif
                </div>
                        <div class="c-productos__imagenes">
                            <div class="c-productos__imagen--producto">
                                <img src="@asset($imagen_producto)" alt="{{ $producto->post_title }}">
                            </div>
                            <div class="c-productos__imagen--tabla">

                                <img src="@asset($imagen_tabla)" alt="{{ $producto->post_title }}">
                            </div>
                    </div>
                </div>
                @endforeach
        </div>
    </div>
</section>

{{-- <header class="burguer u-wrapper">
    <a class="logo" href="{{ home_url('/') }}" title="{{ get_bloginfo('name', 'display') }}"><img src="@asset('images/logo.svg')" class="c-header__logo"></a>
    <input class="menu-btn" type="checkbox" id="menu-btn" />
    <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>
    @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'menu']) !!}
    @endif
</header> --}}
<header class="c-header c-header--fixed">
    <div class="c-header__container u-wrapper u-wrapper--stretch">
        <a class="c-header__logo" href="{{ home_url('/') }}"><img class="js-inlinesvg" src="@asset('images/logo_eu.png')" alt="Forjados Viana"></a>

        <div class="u-hide u-show@tablet-wide">
            <div class="c-hamburger js-hamburger js-menu-toggle">
                <span></span>
                <span></span>
            </div>
        </div>

        <?php wp_nav_menu(array(
        'container' => '',
        'menu' => 'Menu Principal',
        'menu_id' => 'menu-nav',
        'menu_class' => 'c-menu js-menu',
        'link_before' => '',
        'link_after' => ''
        ));
        ?>
    </div>
</header>

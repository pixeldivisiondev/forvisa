
@php
  $blocks = get_field('blocks');
@endphp
<?php foreach ($blocks as $block): ?>

  <?php if ($block['orientacion_blocks'] == 'derecha'): ?>

    <div class="c-interiores u-wrapper">
      <div class="c-interiores__bloques c-interiores__blocks">
        <div class="c-interiores__bloques__texto c-interiores__blocks__text">

          <?php if ($block['titulo_blocks'] != ''): ?>
            <div class="c-interiores__blocks__title">
              <?= $block['titulo_blocks'] ?>
            </div>
          <?php endif ?>

          <div class="c-interiores__bloques__descripcion e-paragraph"><?= $block['contenido_blocks'] ?></div>
        </div>

        <?php if ($block['multimedia_blocks'] == 'imagen'): ?>
          
          <div class="c-interiores__blocks__image">
            <a href="<?= $block['foto_blocks'] ?>" data-lightbox="image-blocks"><img src="<?= $block['foto_blocks'] ?>" alt="<?= $block['titulo_blocks'] ?>"></a>            
          </div>
        
        <?php elseif ($block['multimedia_blocks'] == 'video'): ?>  

          <div class="c-intro__right c-interiores__video">
            <div class="c-intro__video">
              <iframe width="600" height="400" src="//www.youtube.com/embed/{{ $block['video_blocks'] }}" frameborder="0" allow="autoplay" allowfullscreen></iframe>

            </div>
          </div>

        <?php else: ?>

            <div class="c-interiores__sliders">
              <?php foreach ($block['galeria_blocks'] as $key => $slide): ?>
                <div class="c-interiores__slider">                  
                  <a href="<?= $slide ?>" data-lightbox="image-<?= $key ?>"><img src="<?= $slide ?>" /></a>
                </div>
              <?php endforeach ?>
            </div>

        <?php endif ?>

      </div>
    </div>

    <?php else: ?>

      <div class="c-interiores u-wrapper">
        <div class="c-interiores__bloques c-interiores__bloques--reverse c-interiores__blocks">

          <?php if ($block['multimedia_blocks'] == 'imagen'): ?>

            <div class="c-interiores__blocks__image">
            <a href="<?= $block['foto_blocks'] ?>" data-lightbox="image-blocks"><img src="<?= $block['foto_blocks'] ?>" alt="<?= $block['titulo_blocks'] ?>"></a> 
            </div>

          <?php elseif ($block['multimedia_blocks'] == 'video'): ?>  

            <div class="c-intro__right c-interiores__video">
              <div class="c-intro__video">
                <iframe width="600" height="400" src="//www.youtube.com/embed/{{ $block['video_blocks'] }}" frameborder="0" allow="autoplay" allowfullscreen></iframe>

              </div>
            </div>

          <?php else: ?>

            <div class="c-interiores__sliders">
              <?php foreach ($block['galeria_blocks'] as $key => $slide): ?>
                <div class="c-interiores__slider">                  
                  <a href="<?= $slide ?>" data-lightbox="image-<?= $key ?>"><img src="<?= $slide ?>" /></a>
                </div>
              <?php endforeach ?>
            </div>


          <?php endif ?>

          <div class="c-interiores__bloques__texto c-interiores__blocks__text">

            <?php if ($block['titulo_blocks'] != ''): ?>
              <div class="c-interiores__blocks__title"><?= $block['titulo_blocks'] ?></div>
            <?php endif ?>
            
            <div class="c-interiores__bloques__descripcion e-paragraph"><?= $block['contenido_blocks'] ?></div>
          </div>         
        </div>
      </div>

    <?php endif ?>

<?php endforeach ?>


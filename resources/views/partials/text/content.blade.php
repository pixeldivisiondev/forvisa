<div class="c-texto u-wrapper">
  @while(have_posts()) @php the_post() @endphp

  @php the_content() @endphp
  </div>
 @endwhile
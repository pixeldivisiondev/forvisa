<div class="c-hero c-hero__contacto c-hero--centered">
    <div class="c-hero__container u-wrapper">
        <div class=" c-hero__content--copy">
            <h1 class="c-hero__title">{!! get_the_title() !!}</h1>
        </div>
    </div>
    <div class="c-hero__background-image js-object-fit">
        <div class="c-hero__overlay-black"></div>
        <img class="c-hero__background-image--hidden-xs" src="@asset('images/fondo-footer.jpg')" alt="Forvisa Background">
        <img class="c-hero__background-image--hidden" src="@asset('images/fondo-footer.jpg')" alt="Forvisa Background">
    </div>
</div>

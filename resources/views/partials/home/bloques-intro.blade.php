<?php
 $icono_url = 'images/iconos/' . $bloque_intro['modulo_intro_bloques_icono'] . '.svg';
?>
<div class="c-intro__bloques">
    <div class="c-intro__bloques--fila-a">
        <img class="c-intro__bloques--icon" src="@asset($icono_url)" alt="Forvisa icon">
    <div class="c-intro__bloques--pretitle">{{ $bloque_intro['modulo_intro_bloques_titulo'] }}</div>
    </div>
    <div class="c-intro__bloques--fila-b e-paragraph">{!! $bloque_intro['modulo_intro_bloques_descripcion'] !!}</div>
</div>
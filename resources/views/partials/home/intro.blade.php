<?php
    $cta_intro = get_field('modulo_intro_cta');
    $bloques_intro = get_field('modulo_intro_bloques');
    $id_youtube = get_field('id_youtube');
?>
<section class="o-section">
    <div class="u-wrapper">
        <div class="c-intro">
            <div class="c-intro__left">
              <div class="c-intro__pretitle">
                <img class="c-intro__pretitle--icon" src="@asset('images/iconos/coma.svg')" alt="Forvisa icon">
                <div class="c-intro__pretitle--texto">{{ the_field('modulo_intro_pretitulo') }}</div>
              </div>
              <div class="c-intro__titulo">{{ the_field('modulo_intro_titulo') }}</div>

              {{-- bloques introduccion  --}}
              @foreach ($bloques_intro as $bloque_intro)
                @include('partials.home.bloques-intro')
              @endforeach

              <a class="c-intro__cta o-button o-button--inline" target="{{ $cta_intro['target'] }}" href="{{ $cta_intro['url'] }}">{{ $cta_intro['title'] }}</a>
            </div>
            <div class="c-intro__right">
              <div class="c-intro__video">
                <iframe width="500" height="300" src="//www.youtube.com/embed/{{ $id_youtube }}" frameborder="0" allow="autoplay" allowfullscreen></iframe>

              </div>
            </div>
        </div>
    </div>
</section>

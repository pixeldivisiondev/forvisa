<?php
$destacado = $cat['modulo_home_categoria_es_destacado'];
$icono_url = 'images/iconos/' . $cat['modulo_home_categoria_icono'] . '.svg';
$cta = $cat['modulo_home_categoria_cta'];
$foto = $cat['modulo_home_categoria_foto'];
$foto_m = $cat['modulo_home_categoria_foto_mobile'];
$tiene_descarga_pdf = $cat['modulo_home_categoria_tiene_enlace_descarga'];
$pdf_url = $cat['modulo_home_categoria_enlace_descarga'];
if($destacado == 'si') {
 $clase = 'destacado';
} else {
 $clase = 'nodestacado';
}
?>      
    @if ($destacado == 'si')                
    <div class="c-categoria__{{$clase}}" style="background:url(@asset('images/background-categoria-destacado.jpg'));">        
    @else
    <div class="c-categoria__{{$clase}}">  
    @endif       
   
        {{-- share blocks --}}
        <div class="c-categoria__{{$clase}}--container u-wrapper">
            <div class="c-categoria__{{$clase}}--fila-a">
                <img class="c-categoria__icono" src="@asset($icono_url)" alt="" 
                data-aos="fade-down"  
                data-aos-offset="200"
                data-aos-delay="50"
                data-aos-duration="1000">
            <div class="c-categoria__{{$clase}}--titulo">{{ $cat['modulo_home_categoria_titulo'] }}</div>
            </div>
        </div>
        <div class="c-categoria__{{$clase}}--bloque">
            <div class="c-categoria__{{$clase}}--izq">
                <div class="c-categoria__{{$clase}}--subtitulo">{{ $cat['modulo_home_categoria_subtitulo'] }}</div>
                <div class="c-categoria__{{$clase}}--descripcion">{!! $cat['modulo_home_categoria_descripcion'] !!}</div>
            <a class="o-button o-button--inline o-button--{{$clase}}" target="{{ $cta['target'] }}" href="{{ $cta['url'] }}">{{ $cta['title'] }}</a>
            </div>
            

            <div class="c-categoria__{{$clase}}--der" 
                @if ($clase == 'nodestacado')
                data-aos="fade-right"
                @else
                data-aos="fade-left"
                @endif
                data-aos-offset="200"
                data-aos-delay="50"
                data-aos-duration="1000">
            @if ($tiene_descarga_pdf) 
            <a href="{{$pdf_url}}" download title="Descargar PDF">
            @endif  
            <img class="c-categoria__{{$clase}}--foto-desktop" src="{{ $foto['url'] }}" alt="">
            <img class="c-categoria__{{$clase}}--foto-mobile" src="{{ $foto_m['url'] }}" alt="">
           
            @if ($tiene_descarga_pdf) 
            </a>
            @endif  
            </div>

        </div>
    </div>

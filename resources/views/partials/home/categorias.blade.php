<?php 
$home_modulo_categorias = get_field('home_modulo_categorias');
?>

<section class="o-section">
    <div class="c-categoria" style="background:url(@asset('images/fondo-categoria.svg'));">
        @foreach ($home_modulo_categorias as $categoria)
            @foreach ($categoria as $cat)                    
                {{-- categorias  --}}
                @include('partials.home.categoria') 
            @endforeach
        @endforeach       
    </div>    
</section>
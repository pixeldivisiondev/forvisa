<?php     
    $home_modulo_hero = get_field('home_modulo_hero');
?>
<div class="c-hero__sliders">

    @foreach ($home_modulo_hero['hero_home'] as $hero)
        
        <div class="c-hero c-hero--centered">    
            <div class="c-hero__container u-wrapper">
                <div class=" c-hero__content--copy">
                    <div class="c-hero__pretitle">{{ $hero['modulo_hero_pretitulo'] }}</div>
                    <h1 class="c-hero__title">{{  $hero['modulo_hero_titulo'] }}</h1>
                    <a class="o-button o-button--inline" target="{{ $hero['modulo_hero_cta']['target'] }}" href="{{ $hero['modulo_hero_cta']['url'] }}">{{ $hero['modulo_hero_cta']['title'] }}</a>
                </div>
            </div>
            <div class="c-hero__background-image js-object-fit">                 
                <img class="c-hero__background-image--hidden-xs" src="{{ $hero['modulo_hero_fondo']['url'] }}" alt="Forvisa Background">
                <img class="c-hero__background-image--hidden" src="{{ $hero['modulo_hero_fondo']['url'] }}" alt="Forvisa Background">
            </div>    
        </div>     

    @endforeach

</div>
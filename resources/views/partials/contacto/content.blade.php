<section class="c-contacto o-section">
  <div class="c-contacto__container">

    <div class="c-contacto__container__block">
      <div class="c-contacto__content">

        <div class="c-contacto__breadcrumbs">
          @include('partials.breadcrumbs.content')
        </div>

        <p class="c-contacto__title">{{ $contacto_titulo }}</p>

        <div class="c-contacto__blocks">
          <div class="c-contacto__block">
            <div class="c-contacto__block__img"><img src="@asset('images/contacto/icono-direccion.png')"></div>
            <div class="c-contacto__copy">{!! $contacto_direccion !!}</div>
          </div>

          <div class="c-contacto__block">
            <div class="c-contacto__block__img"><img src="@asset('images/contacto/icono-telefono.png')"></div>
            <p class="c-contacto__copy c-contacto__copy--big"><a
                href="tel:{{ $contacto_google_maps }}">{{ $contacto_telefono }}</a></p>
          </div>

          <div class="c-contacto__block">
            <div class="c-contacto__block__img"><img src="@asset('images/contacto/icono-email.png')"></div>
            <p class="c-contacto__copy"><a href="mailto:{{ $contacto_google_maps }}">{{ $contacto_email }}</a></p>
          </div>

        <div class="c-contacto__block">
          <a href="{{ $contacto_google_maps }}"><img class="c-contacto__img-maps"
                                                     src="@asset('images/contacto/google-maps-logo.png')"></a>
        </div>

        </div>

      </div>
    </div>

    <div class="c-contacto__container__block c-contacto__container__block--map">
      <div class="c-contacto__background-image">
        <a href="{{ $contacto_google_maps }}"><img class="c-contacto__block__img"
                                                   src="@asset('images/contacto/bg-map.jpg')"></a>
      </div>
    </div>

  </div>
</section>

<div class="c-hero c-hero__contacto c-hero--centered">
    <div class="c-hero__container u-wrapper">
        <div class=" c-hero__content--copy">
            <div class="c-hero__pretitle">{{ $pretitulo_hero_catalogo }}</div>
            <h1 class="c-hero__title">{{  $titulo_hero_catalogo }}</h1>
        </div>
    </div>
    <div class="c-hero__background-image js-object-fit">
        <div class="c-hero__overlay-black"></div>
        <img class="c-hero__background-image--hidden-xs" src="{{ $fondo_hero_catalogo->url }}" alt="Forvisa Background">
        <img class="c-hero__background-image--hidden" src="{{ $fondo_hero_catalogo->url }}" alt="Forvisa Background">
    </div>
</div>

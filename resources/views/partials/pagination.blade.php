<?php
$prevCategory = App::adjacent_post_by_category('prev');
$nextCategory = App::adjacent_post_by_category('next');
?>
<div class="c-pagination js-pagination">
  @php if($prevCategory !== false): @endphp
    <a href="<?= $prevCategory; ?>"><img class="c-pagination__item c-pagination__item--left" src="@asset('images/iconos/arrow-left-thin.svg')" alt="Anterior"></a>
  @php else: @endphp
    <img class="c-pagination__item c-pagination__item--disabled c-pagination__item--left" src="@asset('images/iconos/arrow-left-thin.svg')" alt="Anterior">
  @php endif; @endphp

  @php if($nextCategory !== false): @endphp
    <a href="<?= $nextCategory; ?>"><img class="c-pagination__item c-pagination__item--right" src="@asset('images/iconos/arrow-right-thin.svg')" alt="Siguiente"></a>
  @php else: @endphp
    <img class="c-pagination__item c-pagination__item--disabled c-pagination__item--right" src="@asset('images/iconos/arrow-right-thin.svg')" alt="Siguiente">
  @php endif; @endphp
</div>

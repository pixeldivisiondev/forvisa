<?php

    $categoria = Categoria::getCategoria($post->ID);
?>

<section class="o-section">
    <div class="c-catalogo">
        <div class="c-catalogo__container u-wrapper">
        <div class="c-catalogo__titulo">{{ the_field('titulo_introduccion_catalogo') }}</div>
            <div class="c-catalogo__descripcion e-paragraph">{!! the_field('descripcion_introduccion_catalogo') !!}</div>
        </div>
    </div>
</section>

<section class="o-section">
    <div class="c-categorias">
        <div class="c-categorias__container">
        @foreach ($categoria as $c)
            @php
                $lang = ICL_LANGUAGE_CODE;

                $link_categoria = get_the_permalink($c->ID) ;
                if ($lang == 'es') {
                    $imagen_distribuidora = 'images/categorias/distribuidora/'. $c->ID . '.jpg';
                } else {
                    $imagen_distribuidora = 'images/categorias/distribuidora/'. get_field('id_categoria_referencia', $c->ID) . '.jpg';
                }

            @endphp
                <div class="c-categorias__item">
                    <a href="{{ $link_categoria }}">
                        <div class="c-categorias__imagen" style="background:url(@asset($imagen_distribuidora))"></div>
                        <div class="c-categorias__titulo">{{ $c->post_title }}</div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</section>

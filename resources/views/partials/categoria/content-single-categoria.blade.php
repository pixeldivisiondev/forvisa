@php
  $categoria = Categoria::getCategoria($post->ID);   
  $productos = Productos::getProductos($post->ID);
@endphp

<div class="c-hero c-hero__catalogo c-hero--centered">    
    <div class="c-hero__container u-wrapper">
        <div class=" c-hero__content--copy">
            <div class="c-hero__pretitle">{{ $post->post_title }}</div>
            <h1 class="c-hero__title">{{ $post->post_title }}</h1>            
        </div>
    </div>
    <div class="c-hero__background-image js-object-fit">  
        @php
            $categoria_overlay = 'images/categorias/hero/'. $post->ID . '.png'; 
        @endphp 
        <div class="c-hero__overlay" style="background:url(@asset($categoria_overlay))"></div>              
        <img class="c-hero__background-image--hidden-xs" src="@asset('images/hero_catalogo.jpg')" alt="Forvisa Background">
        <img class="c-hero__background-image--hidden" src="@asset('images/hero_catalogo.jpg')" alt="Forvisa Background">
    </div>    
</div> 
@include('partials.breadcrumbs.content')
@include('partials.productos.content')

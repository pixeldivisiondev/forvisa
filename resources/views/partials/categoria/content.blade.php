<?php   
    $hero_catalogo = get_field('hero_catalogo');     
?>
<div class="c-hero c-hero__catalogo c-hero--centered">    
    <div class="c-hero__container u-wrapper">
        <div class=" c-hero__content--copy">
            <div class="c-hero__pretitle">{{ $hero_catalogo['pretitulo_hero_catalogo'] }}</div>
            <h1 class="c-hero__title">{{  $hero_catalogo['titulo_hero_catalogo'] }}</h1>            
        </div>
    </div>
    <div class="c-hero__background-image js-object-fit">                 
        <div class="c-hero__overlay" style="background: url(@asset('images/overlay_categoria.png'))"></div>
        <img class="c-hero__background-image--hidden-xs" src="{{ $hero_catalogo['fondo_hero_catalogo']['url'] }}" alt="Forvisa Background">        
        <img class="c-hero__background-image--hidden" src="{{ $hero_catalogo['fondo_hero_catalogo']['url'] }}" alt="Forvisa Background">        
    </div>    
</div> 
@include('partials.breadcrumbs.content')
@include('partials.categoria.categoria')
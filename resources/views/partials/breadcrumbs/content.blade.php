{{-- @dump($post) --}}
@php
    $home_url = get_home_url();
    $current = $post->post_title;
    $type = $post->post_type;  
    $catalogue_url = $home_url . '/catalogo';
    
@endphp

<div class="c-breadcrumbs u-wrapper">
    <ul class="c-breadcrumbs__items">
        <li class="c-breadcrumbs__icon"><img class="c-intro__pretitle--icon" src="@asset('images/iconos/coma.svg')" alt="Forvisa icon"></li>
        <li class="c-breadcrumbs__home"><a class="c-breadcrumbs__link" href="{{ $home_url }}"><?php _e('Inicio', 'breadcrumbs') ?></a></li> 
            @if($type == 'page')            
            <li class="c-breadcrumbs__current">{{ $current }}</li> 
            @else 
            <li class="c-breadcrumbs__category"><a class="c-breadcrumbs__link" href="{{ $catalogue_url }}"><?php _e('Catálogo', 'breadcrumbs') ?></a></li>       
            <li class="c-breadcrumbs__current">{{ $current }}</li> 
            @endif    
    </ul>
</div>
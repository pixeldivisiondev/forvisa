@php
    $herramienta = get_field('bloque_interior_a_imagen_herramienta');
    $foto = get_field('bloque_interior_a_foto');
    $titulo = get_field('bloque_interior_a_titulo');
@endphp

@if ($titulo != '')
<div class="c-interiores">
    <div class="c-interiores__bloques__titulo">{{ $titulo }}</div>
    <div class="c-interiores__bloques">
        <div class="c-interiores__bloques__herramienta">
            <img src="{{ $herramienta['url'] }}" alt="{{ $herramienta['title'] }}"
            data-aos="fade-right"  
            data-aos-offset="200"
            data-aos-delay="50"
            data-aos-duration="1000">
        </div>
        <div class="c-interiores__bloques__texto">
            <div class="c-interiores__bloques__descripcion e-paragraph">{!! the_field('bloque_interior_a_descripcion') !!}</div>
        </div>
        <div class="c-interiores__bloques__foto c-interiores__bloques__foto--der"
            data-aos="fade-left"  
            data-aos-offset="200"
            data-aos-delay="50"
            data-aos-duration="1000">
            <img src="{{ $foto['url'] }}" alt="{{ $foto['title'] }}">
        </div>
    </div>
</div>
@endif
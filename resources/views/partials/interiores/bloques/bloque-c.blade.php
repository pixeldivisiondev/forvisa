@php
    $herramienta = get_field('bloque_interior_c_imagen_herramienta');
    $foto = get_field('bloque_interior_c_foto');
    $cta = get_field('bloque_interior_c_cta');
@endphp

<div class="c-interiores c-interiores__fondo">
    <div class="c-interiores__bloques__titulo">{{ the_field('bloque_interior_c_titulo') }}</div>
    <div class="c-interiores__bloques">
        <div class="c-interiores__bloques__herramienta">
            <img src="{{ $herramienta['url'] }}" alt="{{ $herramienta['title'] }}"
            data-aos="fade-right"
            data-aos-offset="200"
            data-aos-delay="50"
            data-aos-duration="1000">
        </div>
        <div class="c-interiores__bloques__texto">
            <div class="c-interiores__bloques__descripcion e-paragraph">{!! the_field('bloque_interior_c_descripcion') !!}</div>
            <div class="c-interiores__cta">
                <a class="o-button o-button--inline" href="{{ $cta['url'] }}" target="_blank">{{ $cta['title'] }}
                </a>
            </div>
        </div>
        <div class="c-interiores__bloques__foto c-interiores__bloques__foto--der"
            data-aos="fade-left"
            data-aos-offset="200"
            data-aos-delay="50"
            data-aos-duration="1000">
            <img src="{{ $foto['url'] }}" alt="{{ $foto['title'] }}">
        </div>
    </div>
</div>
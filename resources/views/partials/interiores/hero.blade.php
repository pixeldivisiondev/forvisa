<?php   
    $hero_interiores = get_field('hero_interiores');     
?>
<div class="c-hero c-hero__catalogo c-hero--centered">    
    <div class="c-hero__container u-wrapper">
        <div class=" c-hero__content--copy">
            <div class="c-hero__pretitle">{{ $hero_interiores['pretitulo_hero_catalogo'] }}</div>
            <h1 class="c-hero__title">{{  $hero_interiores['titulo_hero_catalogo'] }}</h1>            
        </div>
    </div>
    <div class="c-hero__background-image js-object-fit">  
        <div class="c-hero__overlay-black"></div>
        <img class="c-hero__background-image--hidden-xs" src="{{ $hero_interiores['fondo_hero_catalogo']['url'] }}" alt="Forvisa Background">        
        <img class="c-hero__background-image--hidden" src="{{ $hero_interiores['fondo_hero_catalogo']['url'] }}" alt="Forvisa Background">        
    </div>    
</div> 

{{-- HERO --}}
@include('partials.interiores.hero')
{{-- breadcrumbs --}}
<div class="c-interiores__breadcrumbs">
    @include('partials.breadcrumbs.content')
</div>
{{-- bloque-a --}}
@include('partials.interiores.bloques.bloque-a') 
{{-- bloque-b --}}
@include('partials.interiores.bloques.bloque-b') 
{{-- bloque-c --}}
@include('partials.interiores.bloques.bloque-c') 
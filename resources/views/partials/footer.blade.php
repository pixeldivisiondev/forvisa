<footer class="c-footer" style="background:url(@asset('images/fondo-footer.jpg'));">
  <div class="u-wrapper">
    <div class="c-footer__copy">{!! $opciones_generales['copy_footer'] !!}</div>
    <div class="c-footer__container">
      <div class="c-footer__izq">
        <a class="logo" href="{{ home_url('/') }}" title="{{ get_bloginfo('name', 'display') }}"><img src="@asset('images/logo.svg')" class="c-footer__logo"></a>
        @if (has_nav_menu('legal_menu'))
        {!! wp_nav_menu(['theme_location' => 'legal_menu', 'menu_class' => 'c-footer__menu-izq']) !!}
        @endif
      </div>
      <div class="c-footer__der">

        <div class="c-footer__container--items">
          <div class="c-footer__container--item">
            <img src="@asset('images/iconos/marker.svg')" alt="Dirección"
            data-aos="zoom-in"
            data-aos-offset="200"
            data-aos-delay="50"
            data-aos-duration="1000">
            {{ $opciones_generales['direccion_footer'] }}
          </div>
          <div class="c-footer__container--item">
            <img src="@asset('images/iconos/smartphone.svg')" alt="Teléfono"
            data-aos="zoom-in"
            data-aos-offset="200"
            data-aos-delay="50"
            data-aos-duration="1000">
            {{ $opciones_generales['telefono_footer'] }}
          </div>
          <div class="c-footer__container--item">
            <img src="@asset('images/iconos/envelope.svg')" alt="Email"
            data-aos="zoom-in"
            data-aos-offset="200"
            data-aos-delay="50"
            data-aos-duration="1000">
            <a href="mailto:{{ $opciones_generales['email_footer'] }}">{{ $opciones_generales['email_footer'] }}</a>
          </div>
        </div>

        @if (has_nav_menu('footer_menu'))
        {!! wp_nav_menu(['theme_location' => 'footer_menu', 'menu_class' => 'c-footer__menu-der']) !!}
        @endif

        <div id="ayudas" class="lity-hide c-footer__ayudas">{!! $opciones_generales['ayudas_footer'] !!}</div>

      </div>
    </div>
    
    @if ($opciones_generales['ayudas_footer'] != '')
    <div class="c-footer__container-ayudas">
      {!! $opciones_generales['ayudas_footer'] !!}
    </div>
     @endif

  </div>
</footer>
<div class="c-footer__container--bottom">{{ $opciones_generales['copyright_footer'] }}</div>
<a class="c-back-to-top js-back-to-top" href="#" data-offset="100" data-duration="400">
    <svg class="icon" viewBox="0 0 16 16">
      <title>Go to top of page</title>
      <g stroke-width="1" stroke="currentColor">
        <polyline fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="15.5,11.5 8,4 0.5,11.5 "></polyline>
      </g>
    </svg>
</a>

{{--
  Template Name: Contacto Template
--}}

@extends('layouts.app')
  @section('content')
    @include('partials.contacto.hero')
    @include('partials.contacto.content')
  @endsection

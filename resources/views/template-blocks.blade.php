{{--
  Template Name: Blocks Template
--}}

@extends('layouts.app')

@section('content')
  @include('partials.principales.content')
  @include('partials.blocks.content')

@endsection

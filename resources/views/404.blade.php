@extends('layouts.app')

@section('content')
  @if (!have_posts())
  <div class="c-hero c-hero__contacto c-hero--centered">
    <div class="c-hero__container u-wrapper">
        <div class=" c-hero__content--copy">
            <h1 class="c-hero__title">Error 404</h1>
        </div>
    </div>
    <div class="c-hero__background-image js-object-fit">
        <div class="c-hero__overlay-black"></div>
        <img class="c-hero__background-image--hidden-xs" src="@asset('images/fondo-footer.jpg')" alt="Forvisa Background">
        <img class="c-hero__background-image--hidden" src="@asset('images/fondo-footer.jpg')" alt="Forvisa Background">
    </div>
</div>
    <div class="c-texto u-wrapper">
      <p style="font-size: 24px;line-height: 30px;margin-bottom: 36px;"> <strong>¡Vaya! Esta página no se encuentra.</strong> <br>
      	Parece que no hay nada en esa ubicación.
      </p>

    <a class="o-button o-button--inline o-button--nodestacado" target="" href="<?php echo get_site_url(); ?>" style="color: #fff;">Ir al Inicio</a>
    </div>
  @endif
@endsection


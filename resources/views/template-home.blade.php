{{--
  Template Name: Home Template
--}}

@extends('layouts.app')

@section('content')
  {{-- hero  --}}
  @include('partials.home.hero')  
  {{-- introduccion  --}}
  @include('partials.home.intro')  
  {{-- categorias  --}}
  @include('partials.home.categorias') 
@endsection

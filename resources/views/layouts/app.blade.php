<!doctype html>
<html {!! get_language_attributes() !!}>
@include('partials.head')
<body @php body_class() @endphp>
@php do_action('get_header') @endphp
<div id="page" class="site">
   <div class="site-content-contain">
       <div id="content" class="site-content">
            <div class="js-main-header">
              @include('partials.header')
            </div>
           <div class="wrap">
               <div id="primary" class="content-area">
                   <main id="main" class="site-main" role="main">   
                       @yield('content')
                   </main>
               </div>
           </div>
       </div>
   </div>
</div>
@php do_action('get_footer') @endphp
@section('footer')
   @include('partials.footer')
@show
@php wp_footer() @endphp
</body>
</html>
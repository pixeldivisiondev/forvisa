<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public function opcionesGenerales()
    {
        global $wpdb;
        $data = array();
        $results = $wpdb->get_results("SELECT option_name FROM `wp_options` WHERE `option_name` LIKE 'options_%'");
        foreach ($results as $result):
            $trimmed = str_replace('options_', '', $result->option_name);
            $data[$trimmed] = get_field($trimmed, 'option');
        endforeach;
        return $data;
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    public static function adjacent_post_by_category($direction) {
        $postIDs = array();
        $currentPost = get_the_ID();

        $postArray = get_posts( array(
            'posts_per_page'	=> '-1',
            'post_type' => array('categoria'),
            'orderby'=>'menu_order',
            'order'=> 'ASC'
        ));

        foreach ( $postArray as $thepost ):
            $postIDs[] = $thepost->ID;
        endforeach;

        $currentIndex = array_search( $currentPost, $postIDs );
        $prevID = $postIDs[ $currentIndex - 1 ];
        $nextID = $postIDs[ $currentIndex + 1 ];

        if( $direction == 'next' AND $nextID ):
            return get_permalink($nextID);
        elseif( $direction == 'prev' AND $prevID ):
            return get_permalink($prevID);
        else:
            return false;
        endif;
    }
}

<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Categoria extends Controller
{

    // Pass on all fields from Advanced Custom Fields to the view
    protected $acf = true;

    public static function getCategoria()
    {

    	$args  = [
    		'posts_per_page'	=> '-1',
            'post_type'		    => array('categoria'),
            'orderby'           => 'menu_order',
    		'order'				=> 'ASC'    		
    	];
    	$query = new \WP_Query($args);
    	return $query->posts;
    }
}
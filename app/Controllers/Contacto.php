<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Contacto extends Controller
{

    protected $template = 'template-contacto';

    // Pass on all fields from Advanced Custom Fields to the view
    protected $acf = true;
}

<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Productos extends Controller
{

    protected $template = 'page-productos';

    // Pass on all fields from Advanced Custom Fields to the view
    protected $acf = true;

    public static function getProductos($id_categoria)
    {

    	$colegio = get_field('cursos_colegio');
    	$args  = [
    		'posts_per_page'	=> '-1',
    		'post_type'		    => array('productos'),
			'order'				=> 'ASC',
			'meta_key'			=> 'categoria_producto',
			'meta_value'		=> $id_categoria


    	];
		$query = new \WP_Query($args);
    	return $query->posts;
    }
}
